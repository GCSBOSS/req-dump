#!node
const Eclipt = require('eclipt');

let cli = new Eclipt('req-dump', {
    condensed: [ 'c', 'Omit some ordinary details and headers' ],
    port: [ 'p', 'Port where the server will be listening', 'number', 8888 ],
    status: [ 's', 'Status code to be sent in every response', 'code', 200 ]
});

const { condensed: CONDENSED, port: PORT, status: STATUS } = cli.execute().data,

    TRIVIAL_HEADERS = ['host', 'connection', 'content-length', 'accept',
        'accept-encoding', 'date', 'user-agent', 'cache-control'];

function parseHeaders(headers){
    var output = CONDENSED ? '' : '\n-- Headers ----------';
    for(var k in headers){
        if(CONDENSED && TRIVIAL_HEADERS.includes(k))
            continue;
        output += '\n' + k + ': ' + headers[k];
    }
    return output;
}

function parseBody(req){
    return new Promise(function(done){
        var body = '';
        req.on('data', buf => body += buf.toString());
        req.on('end', () => {
            if(CONDENSED)
                body = '\n---\n' + body.replace(/\s+/, ' ');
            else
                body = '\n-- Body -------------\n' + body + '\n-- End Body ---------';
            done(body);
        });
    });
}

const server = require('http').createServer(async function(req, res){

    var body = await parseBody(req);
    var headers = parseHeaders(req.headers);

    let date = new Date();
    var headline = CONDENSED
        ? '\n[' + date.toISOString().replace('T', ' ').replace(/\..*/, '') + ']'
        : '\n[Incoming Request] ' + String(date);

    var endpoint = CONDENSED
        ? '\n' + req.method + ' ' + req.url
        : '\nMethod: ' + req.method + '\nURL: ' + req.url;

    console.log(headline + endpoint + headers + body + '\n\n');

    res.statusCode = STATUS;
    res.end();
});

server.listen(PORT);

console.log('\nReq Dump (v0.1.0) is listening on port ' + PORT);
